# APP startup

- cd into it
- to start it up use **npm run dev**
- or for storybook use(deprecated) **npm run storybook**

# API startup

- cd into api folder
- read README

# Sensor simulation script startup

- cd into sensorApp folder
- run **node sensorQuery.js**
