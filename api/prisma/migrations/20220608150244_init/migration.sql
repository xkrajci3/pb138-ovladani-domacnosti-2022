-- CreateEnum
CREATE TYPE "LightType" AS ENUM ('RGB', 'DIMABLE', 'RGBDIMABLE', 'BASIC');

-- CreateEnum
CREATE TYPE "SensorType" AS ENUM ('HEAT', 'LIGHT', 'MOTION');

-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Room" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "Room_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Light" (
    "id" SERIAL NOT NULL,
    "isOn" BOOLEAN NOT NULL,
    "red" INTEGER NOT NULL,
    "green" INTEGER NOT NULL,
    "blue" INTEGER NOT NULL,
    "opacity" DOUBLE PRECISION NOT NULL,
    "type" "LightType" NOT NULL DEFAULT E'BASIC',
    "name" TEXT NOT NULL,
    "roomName" TEXT NOT NULL,

    CONSTRAINT "Light_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Heating" (
    "id" SERIAL NOT NULL,
    "state" BOOLEAN NOT NULL,
    "target" INTEGER NOT NULL,
    "roomId" INTEGER NOT NULL,

    CONSTRAINT "Heating_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Blinds" (
    "id" SERIAL NOT NULL,
    "state" DOUBLE PRECISION NOT NULL,
    "roomId" INTEGER NOT NULL,

    CONSTRAINT "Blinds_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Sensor" (
    "id" SERIAL NOT NULL,
    "type" "SensorType" NOT NULL DEFAULT E'LIGHT',
    "roomId" INTEGER NOT NULL,

    CONSTRAINT "Sensor_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Measurement" (
    "id" SERIAL NOT NULL,
    "value" DOUBLE PRECISION NOT NULL,
    "timeStamp" TIMESTAMP(3) NOT NULL,
    "sensorId" INTEGER NOT NULL,

    CONSTRAINT "Measurement_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Room_name_key" ON "Room"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Light_roomName_name_key" ON "Light"("roomName", "name");

-- CreateIndex
CREATE UNIQUE INDEX "Sensor_roomId_type_key" ON "Sensor"("roomId", "type");

-- AddForeignKey
ALTER TABLE "Light" ADD CONSTRAINT "Light_roomName_fkey" FOREIGN KEY ("roomName") REFERENCES "Room"("name") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Heating" ADD CONSTRAINT "Heating_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "Room"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Blinds" ADD CONSTRAINT "Blinds_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "Room"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Sensor" ADD CONSTRAINT "Sensor_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "Room"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Measurement" ADD CONSTRAINT "Measurement_sensorId_fkey" FOREIGN KEY ("sensorId") REFERENCES "Sensor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
