import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seeds a storage
export const seedPool = async () => {
    await prisma.room.create({
        data: {
            name: 'pool',
            lights: {
                create: 
                [{
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 0.8,
                  type: LightType.DIMABLE,
                  name: 'Pool Main Lights',
                  isOn: false,
                }, {
                  red: 40,
                  green: 90,
                  blue: 20,
                  opacity: 1,
                  type: LightType.RGBDIMABLE,
                  name: 'Pool Theme Lights',
                  isOn: false,
                }]

            },
            heatings: {
                create: {
                    state: true,
                    target: 20,
                }
            },
            blinds: {
                create: {
                    state: 0,
                }
            },
            sensors: {
                create: [{
                    type: SensorType.MOTION,
                }, {
                    type: SensorType.HEAT,
                }]
            }
        },
    });
}
