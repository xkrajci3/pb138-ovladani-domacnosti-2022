import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seeds a bathroom
export const seedBathroom = async () => {
    await prisma.room.create({
        data: {
            name: 'bathroom',
            lights: {
                create: {
                    red: 253,
                    green: 216,
                    blue: 53,
                    opacity: 1,
                    type: LightType.BASIC,
                    name: 'Bathroom light',
                    isOn: false,
                }
            },
            heatings: {
              create: {
                state: true,
                target: 20,
              }
            },
            blinds: {
              create: {
                state: 0,
              }
            },
            sensors: {
                create: {
                    type: SensorType.HEAT,
                }
            }
        },
    });
}
