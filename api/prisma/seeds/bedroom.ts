import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seeds a bedroom
export const seedBedroom = async () => {
    await prisma.room.create({
        data: {
            name: 'bedroom',
            lights: {
                create: [{
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 1,
                  type: LightType.BASIC,
                  name: 'Main light',
                  isOn: true,
                },
                {
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 0.7,
                  type: LightType.DIMABLE,
                  name: 'Night light left',
                  isOn: false,
                },
                {
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 0.5,
                  type: LightType.DIMABLE,
                  name: 'Night light right',
                  isOn: false,
                }]

            },
            heatings: {
                create: {
                    state: true,
                    target: 20,
                }
            },
            blinds: {
                create: {
                    state: 0,
                }
            },
            sensors: {
                create: [{
                    type: SensorType.LIGHT,
                }, {
                    type: SensorType.HEAT,
                }]
            }
        },
    });
}
