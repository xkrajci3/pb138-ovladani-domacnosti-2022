import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seeds a storage
export const seedStorage = async () => {
    await prisma.room.create({
        data: {
            name: 'storage',
            lights: {
                create: 
                {
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 0.5,
                  type: LightType.DIMABLE,
                  name: 'Storage Light',
                  isOn: false,
                }

            },
            heatings: {
              create: {
                state: true,
                target: 20,
              }
            },
            blinds: {
                create: {
                    state: 0,
                }
            },
            sensors: {
                create: {
                    type: SensorType.HEAT,
                }
            }
        },
    });
}
