import {seedBathroom} from "./bathroom";
import {seedKitchen} from "./kitchen";
import {seedBedroom} from "./bedroom";
import {seedEntryHall} from "./entryHall";
import {seedLivingRoom} from "./livingRoom";
import {seedStorage} from "./storage";
import {seedPool} from "./pool";

export {
    seedBathroom,
    seedKitchen,
    seedLivingRoom,
    seedEntryHall,
    seedBedroom,
    seedStorage,
    seedPool,
}