import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seeds a entry hall
export const seedEntryHall = async () => {
    await prisma.room.create({
        data: {
            name: 'entryhall',
            lights: {
                create: {
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 1,
                  type: LightType.BASIC,
                  name: 'Main light',
                  isOn: true,
                }
            },
            heatings: {
              create: {
                state: true,
                target: 20,
              }
            },
            blinds: {
                create: {
                    state: 0,
                }
            },
            sensors: {
                create: [{
                    type: SensorType.HEAT,
                }, {
                    type: SensorType.MOTION,
                }]
            }
        },
    });
}
