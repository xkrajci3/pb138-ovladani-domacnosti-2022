import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seed a living room
export const seedLivingRoom = async () => {
    await prisma.room.create({
        data: {
            name: 'livingroom',
            lights: {
                create: [{
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 1,
                  type: LightType.BASIC,
                  name: 'Main light',
                  isOn: false,
                },
                {
                  red: 40,
                  green: 70,
                  blue: 90,
                  opacity: 0.6,
                  type: LightType.RGBDIMABLE,
                  name: 'Theme light',
                  isOn: true,
                }]
            },
            heatings: {
                create: {
                    state: true,
                    target: 20,
                }
            },
            blinds: {
                create: {
                    state: 0,
                }
            },
            sensors: {
                create: [{
                    type: SensorType.LIGHT,
                }, {
                    type: SensorType.HEAT,
                }, {
                    type: SensorType.MOTION,
                }]
            }
        },
    });
}
