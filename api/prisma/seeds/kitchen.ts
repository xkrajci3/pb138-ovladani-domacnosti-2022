import {LightType, SensorType} from "../../src/types/database";
import prisma from "../../client";

//Seeds a kitchen
export const seedKitchen = async () => {
    await prisma.room.create({
        data: {
            name: 'kitchen',
            lights: {
                create: [{
                  red: 253,
                  green: 216,
                  blue: 53,
                  opacity: 1,
                  type: LightType.BASIC,
                  name: 'Main light',
                  isOn: false,
                }, {
                  red: 200,
                  green: 100,
                  blue: 50,
                  opacity: 1,
                  type: LightType.RGB,
                  name: 'Theme Refridgerator Light',
                  isOn: true,
                }]
            },
            heatings: {
                create: {
                    state: true,
                    target: 20.0,
                }
            },
            blinds: {
              create: {
                state: 0,
              }
            },
            sensors: {
                create: [{
                    type: SensorType.MOTION,
                }, {
                    type: SensorType.HEAT,
                }]
            }
        },
    });
}
