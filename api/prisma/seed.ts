import {PrismaClient} from '@prisma/client'
import {seedBathroom, seedBedroom, seedEntryHall, seedKitchen, seedLivingRoom, seedPool, seedStorage} from "./seeds";

const prisma = new PrismaClient()

const main = async () => {
  console.log('start seeding …')
  await seedBathroom();
  console.log('Bathroom seeded')
  await seedKitchen();
  console.log('Kitchen seeded')
  await seedLivingRoom();
  console.log('Living room seeded')
  await seedEntryHall();
  console.log('Entry hall seeded')
  await seedBedroom();
  console.log('Bedroom seeded')
  await seedStorage();
  console.log('Storage seeded')
  await seedPool();
  console.log('Pool seeded')
  const allSensors = await prisma.sensor.findMany({
    select: {
      id: true,
    },
  });
  allSensors.map(async (id) => {
    const createdMeasurement = await prisma.sensor.update({
      where: {
        id: id.id,
      },
      data: {
        measurements: {
          create: {
            value: 20,
            timeStamp: new Date,
          }
        }
      }
    });
  });
  console.log('seeding done');
}

main()
  .catch(e => {
    console.error('foo', e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
