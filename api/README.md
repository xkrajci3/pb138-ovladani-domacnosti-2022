<div>
  <img width="512" src="https://raw.githubusercontent.com/grdnmsz/prisma-docker/master/banner.png" alt="prisma-docker">
</div>

## Api for PB138 project "Ovládanie domácnosti"

This api uses template project for bootstraping a back-end application with nodejs (express), postgresql and prisma within a docker container.
Template source repository: https://github.com/grdnmsz/prisma-docker.git
## Getting started
- Build, fetch and run docker containers
```
docker-compose up -d
```

- Seed mock-up database
```
npx prisma db push
npx prisma db seed
```
- In case that does not work, try
```
npx prisma migrate reset
```

## Tech & framework used
- [Docker](https://www.docker.com/), an open platform for developing, shipping, and running applications
- [Prisma](https://www.prisma.io/), a new kind of ORM for Node.js and Typescript
- [Express](https://expressjs.com/) a fast unopinionated, minimalist web framework for Node.js
- [PostgreSQL](https://www.postgresql.org/), an open source relational database
