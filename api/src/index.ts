import { WebSocketServer } from 'ws';
import { app } from './http-server';
import { createServer } from "http";


const server = createServer();
const PORT = 5000;
const wss = new WebSocketServer({ server: server });
const clients = new Map();

export const getClients = () => {
    return clients;
}

server.on('request', app);

wss.on('connection', (ws: any) => {
    const id = uuidv4();
    const color = Math.floor(Math.random() * 360);
    const metadata = { id, color };

    clients.set(ws, metadata);

    ws.on('message', (messageAsString: any) => {
        const message = JSON.parse(messageAsString);
        const metadata = clients.get(ws);

        message.sender = metadata.id;
        message.color = metadata.color;

        [...clients.keys()].forEach((client) => {
            client.send(JSON.stringify(message));
        });
    });
});

/*wss.on("close", () => {
    clients.delete(ws);
});*/

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

console.log("wss up");

server.listen(PORT, () => {
    console.log(`http/ws server listening on ${PORT}`);
});
