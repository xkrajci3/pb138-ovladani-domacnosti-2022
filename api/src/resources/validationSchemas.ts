import * as yup from "yup";

export const roomNameQuerySchema = yup.object().shape({
  roomName: yup.string().required(),
});

export const lightNameBodySchema = yup.object().shape({
  currentLightName: yup.string().required(),
  newLightName: yup.string().required(),
});

export const blindsBodySchema = yup.object().shape({
  state: yup.number().required(),
});

export const heatingBodySchema = yup.object().shape({
  state: yup.boolean().required(),
  targetTemperature: yup.number().required(),
});

export const lightBodySchema = yup.object().shape({
  name: yup.string().required(),
  isOn: yup.boolean().required(),
  state: yup.object().shape({
    r: yup.number().required(),
    g: yup.number().required(),
    b: yup.number().required(),
    a: yup.number().required(),
  })
});

export const temperatureMeasurementBodySchema = yup.object().shape({
  value: yup.number().required(),
  roomId: yup.number().required(),
});

export const switchAllQuerySchema = yup.object().shape({
  state: yup.boolean().required(),
});
