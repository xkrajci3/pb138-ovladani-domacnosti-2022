import prisma from "../../client";
import {SensorType} from "../types/database";

export const findRoomIdByRoomName = async (roomName: string) => {
  return await prisma.room.findUnique({
    where: {
      name: roomName,
    },
    select: {
      id: true,
    },
  });
}

export const updateBlinds = async (roomId: number,
                                   state: number) => {
  return await prisma.blinds.updateMany({
    where: {
      roomId: roomId,
    },
    data: {
      state: state,
    }
  });
}

export const updateHeating = async (roomId: number,
                                    state: boolean,
                                    targetTemperature: number) => {
  return await prisma.heating.updateMany({
    where: {
      roomId:roomId,
    },
    data: {
      state: state,
      target: targetTemperature,
    }
  });
}

export const updateLight = async (roomName: string,
                                   lightName: string,
                                   red: number,
                                   green: number,
                                   blue: number,
                                   alpha: number,
                                   isOn: boolean) => {
  return await prisma.light.update({
    where: {
      roomName_name: {
        roomName: roomName,
        name: lightName,
      },
    },
    data: {
      red: red,
      green: green,
      blue: blue,
      opacity: alpha,
      isOn: isOn,
    }
  });
}

export const getRoomBlinds = async (roomName: string) => {
  return await prisma.room.findUnique({
    where: {
      name: roomName,
    },
    select: {
      blinds: {
        select: {
          state: true,
        }
      }
    }
  });
}

export const getRoomHeating = async (roomName: string) => {
  return await prisma.room.findUnique({
    where: {
      name: roomName,
    },
    select: {
      id: true,
      heatings: {
        select: {
          state: true,
          target: true,
        }
      }
    }
  });
}

export const getRoomTemperature = async (roomId: number) => {
  return await prisma.sensor.findUnique({
    where: {
      roomId_type: {
        roomId: roomId,
        type: SensorType.HEAT,
      },
    },
    select: {
      measurements: {
        select: {
          value: true,
        },
        orderBy: {
          id: 'desc',
        },
        take: 1,
      },
    },
  });
}

export const getRoomLights = async (roomName: string) => {
  return await prisma.room.findUnique({
    where: {
      name: roomName,
    },
    select: {
      lights: {
        select: {
          name: true,
          isOn: true,
          red: true,
          green: true,
          blue: true,
          opacity: true,
          type: true,
        },
      },
    },
  });
}

export const getRoomNames = async () => {
  return await prisma.room.findMany({
    select: {
      name: true,
    },
  });
}

export const getRoomTemperatureStats = async () => {
  return await prisma.room.findMany({
    select: {
      id: true,
      sensors: {
        where: {
          type: SensorType.HEAT,
        },
        select: {
          measurements: {
            orderBy: {
              id: 'desc',
            },
            take: 1,
          },
        },
        orderBy: {
          id: 'desc',
        },
        take: 1,
      },
      heatings: {
        select: {
          target: true,
          state: true,
        },
        orderBy: {
          id: 'desc',
        },
        take: 1,
      },
    }
  });
}

export const createTemperatureMeasurement = async (roomId: number, value: number) => {
  return await prisma.sensor.update({
    where: {
      roomId_type: {
        roomId: roomId,
        type: SensorType.HEAT,
      }
    },
    data: {
      measurements: {
        create: {
          value: parseFloat(value.toFixed(1)),
          timeStamp: new Date,
        }
      }
    }
  });
}

export const getRoomTargetTemperature = async (roomId: number) => {
  return await prisma.room.findUnique({
    where: {
      id: roomId,
    },
    select: {
      heatings: {
        select: {
          id: true,
          target: true,
        },
        take: 1,
      },
    },
  });
}

export const switchOffHeating = async (heatingId: number) => {
  return await prisma.heating.update({
    where: {
      id: heatingId,
    },
    data: {
      state: false,
    },
  })
}

export const switchOnHeating = async (heatingId: number) => {
  return await prisma.heating.update({
    where: {
      id: heatingId,
    },
    data: {
      state: true,
    },
  })
}

export const findLight = async (roomName: string, lightName: string) => {
  return await prisma.light.findUnique({
    where: {
      roomName_name: {
        roomName: roomName,
        name: lightName,
      },
    },
  });
}

export const updateLightName = async (roomName: string, currentName: string, newName: string) => {
  return await prisma.light.update({
    where: {
      roomName_name: {
        roomName: roomName,
        name: currentName,
      }
    },
    data: {
      name: newName,
    },
  });
}

export const switchAllLights = async (state: boolean) => {
  return await prisma.light.updateMany({
    data: {
      isOn: state,
    },
  })
}

export const switchAllHeating = async (state: boolean) => {
  return await prisma.heating.updateMany({
    data: {
      state: state,
    },
  })
}

export const switchAllBlinds = async (state: boolean) => {
  return await prisma.blinds.updateMany({
    data: {
      state: state ? 10 : 0,
    },
  })
}
