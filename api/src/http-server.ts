import bodyParser from 'body-parser';
import express, { Express, RequestHandler } from 'express';
import { getClients } from "./index";
import {HeatingResponse, LightResponse, RoomTemperatureResponse} from "./types/server";
import {LightType} from "./types/database";
import * as yup from 'yup';
import cors from "cors";
import {
  blindsBodySchema,
  heatingBodySchema,
  lightBodySchema, lightNameBodySchema,
  roomNameQuerySchema, switchAllQuerySchema,
  temperatureMeasurementBodySchema
} from "./resources/validationSchemas";
import {
  findRoomIdByRoomName,
  getRoomNames,
  getRoomBlinds,
  getRoomHeating,
  getRoomLights,
  getRoomTemperature,
  getRoomTemperatureStats,
  updateBlinds,
  updateHeating,
  updateLight,
  createTemperatureMeasurement,
  updateLightName,
  findLight,
  switchAllLights,
  switchAllHeating,
  switchAllBlinds, getRoomTargetTemperature, switchOffHeating, switchOnHeating
} from "./resources/databaseAccess";

export const app: Express = express();
app.use(cors());
app.use(bodyParser.json() as RequestHandler);
app.use(bodyParser.urlencoded({ extended: true }) as RequestHandler);

app.get('/', async (req, res) => {
  // TESTING ENVIRONMENT
  res.status(100).send(JSON.stringify("test test 1 2 3"));
  const clients = getClients();
  [...clients.keys()].forEach((client) => {
    client.send(JSON.stringify('http to ws test'));
  });
});

app.put('/sensors/measurement', async (req, res) => {
  try {
    // Request validation
    const body = await temperatureMeasurementBodySchema.validate(req.body);
    // Database access
    const createdMeasurement = await createTemperatureMeasurement(body.roomId, body.value);
    if (!createdMeasurement) {
      res.status(500).send('Internal server error');
      return;
    }
    const room = await getRoomTargetTemperature(body.roomId);
    if (!room) {
      res.status(500).send('Internal server error');
      return;
    }
    if (room.heatings[0].target <= body.value) {
      const switchedOffHeating = await switchOffHeating(room.heatings[0].id);
      if (!switchedOffHeating) {
        res.status(500).send('Internal server error');
        return;
      }
    } else {
      const switchedOnHeating = await switchOnHeating(room.heatings[0].id);
      if (!switchedOnHeating) {
        res.status(500).send('Internal server error');
        return;
      }
    }
    // Notify app about temperature change
    const clients = getClients();
    [...clients.keys()].forEach((client) => {
      client.send(JSON.stringify('Temperature has been updated.'));
    });
    // Everything is successful
    res.status(200).send('Measurement added successfully');
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.get('/sensors/room/temperature', async (req, res) => {
  try {
    // Database access
    const roomStats = await getRoomTemperatureStats();
    // Prepare response object
    const roomTemperatureStats: RoomTemperatureResponse[] = roomStats.map((stat) => {
      if (stat.heatings.length === 0) {
        return {
          roomId: stat.id,
          temperature: 0,
          target: 0,
          isOn: false,
        }
      }
      return {
        roomId: stat.id,
        temperature: stat.sensors[0].measurements[0].value,
        target: Number(stat.heatings[0].target),
        isOn: stat.heatings[0].state,
      }
    });
    // Everything is successful
    res.status(200).send(JSON.stringify(roomTemperatureStats));
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.get('/api/rooms', async (req, res) => {
  try {
    // Database access
    const rooms = await getRoomNames();
    // Everything is successful
    res.status(200).send(JSON.stringify(rooms));
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.get('/api/room/lights', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    // Database access
    const rawLights = await getRoomLights(query.roomName);
    if (!rawLights) {
      res.status(500).send('Internal server error');
      return;
    }
    // Prepare response object
    const lights: LightResponse[] = rawLights.lights.map((light): LightResponse => {
      return {
        name: light.name,
        isOn: light.isOn,
        type: light.type as LightType,
        state: {
          r: light.red,
          g: light.green,
          b: light.blue,
          a: light.opacity,
        },
      }
    })
    // Everything is successful
    res.status(200).send(JSON.stringify(lights));
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.get('/api/room/heating', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    // Database access
    const room = await getRoomHeating(query.roomName);
    if (!room) {
      res.status(500).send('Internal server error');
      return;
    }
    const temperature = await getRoomTemperature(room.id);
    if (!temperature) {
      res.status(500).send('Internal server error');
      return;
    }
    // Prepare response object
    const heating: HeatingResponse = {
      state: room.heatings[0].state,
      targetTemperature: room.heatings[0].target,
      actualTemperature: temperature.measurements[0].value,
    };
    // Everything is successful
    res.status(200).send(JSON.stringify(heating));
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.get('/api/room/blinds', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    // Database access
    const room = await getRoomBlinds(query.roomName)
    if (!room) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send(JSON.stringify({state: room.blinds[0].state}));
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.put('/api/room/light', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    const body = await lightBodySchema.validate(req.body);
    // Database access
    const updatedLight = await updateLight(query.roomName, body.name, body.state.r, body.state.g, body.state.b,
      body.state.a, body.isOn);
    if (!updatedLight) {
      res.status(500).send('Internal server error a');
      return;
    }
    // Everything is successful
    res.status(200).send('Light has been updated');
  } catch {
    res.status(500).send('Internal server error b');
  } finally {
    res.end();
  }
});

app.put('/api/room/heating', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    const body = await heatingBodySchema.validate(req.body);
    // Database access
    const room = await findRoomIdByRoomName(query.roomName);
    if (!room) {
      res.status(500).send('Internal server error');
      return;
    }
    const updatedHeating = await updateHeating(room.id, body.state, body.targetTemperature);
    if (!updatedHeating) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send('Heating updated successfully');
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.put('/api/room/blinds', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    const body = await blindsBodySchema.validate(req.body)
    // Database access
    const room = await findRoomIdByRoomName(query.roomName);
    if (!room) {
      res.status(500).send('Internal server error');
      return;
    }
    const updatedBlinds = await updateBlinds(room.id, body.state);
    if (!updatedBlinds) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send('Blinds updated successfully');
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.put('/api/room/light/name', async (req, res) => {
  try {
    // Request validation
    const query = await roomNameQuerySchema.validate(req.query);
    const body = await lightNameBodySchema.validate(req.body);
    // Database access
    const lightFound = await findLight(query.roomName, body.newLightName);
    if (lightFound !== null) {
      res.status(400).send(`Light name ${body.newLightName} already exists in room ${query.roomName}`);
      return;
    }
    const updatedLight = await updateLightName(query.roomName, body.currentLightName, body.newLightName);
    if (!updatedLight) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send('Light name updated successfully');
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.put('/api/all/lights', async (req, res) => {
  try {
    // Request validation
    const query = await switchAllQuerySchema.validate(req.query);
    // Database access
    const switchedLights = await switchAllLights(query.state);
    if (!switchedLights) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send(`All lights switched ${query.state ? "ON" : "OFF"} successfully`);
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.put('/api/all/heating', async (req, res) => {
  try {
    // Request validation
    const query = await switchAllQuerySchema.validate(req.query);
    // Database access
    const switchedHeating = await switchAllHeating(query.state);
    if (!switchedHeating) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send(`All heating switched ${query.state ? "ON" : "OFF"} successfully`);
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});

app.put('/api/all/blinds', async (req, res) => {
  try {
    // Request validation
    const query = await switchAllQuerySchema.validate(req.query);
    // Database access
    const switchedBlinds = await switchAllBlinds(query.state);
    if (!switchedBlinds) {
      res.status(500).send('Internal server error');
      return;
    }
    // Everything is successful
    res.status(200).send(`All blinds switched to ${query.state ? "OPEN" : "CLOSED"} successfully`);
  } catch {
    res.status(500).send('Internal server error');
  } finally {
    res.end();
  }
});
