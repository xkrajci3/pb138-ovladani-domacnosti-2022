import {LightType} from "./database";

export interface SensorUpdate {
    sensorId: number,
    value: number,
}

export interface RoomTemperatureResponse {
    roomId: number,
    temperature: number,
    target: number,
    isOn: boolean,
}

export interface LightResponse {
  name: string,
  isOn: boolean,
  type: LightType,
  state: {
    r: number,
    g: number,
    b: number,
    a: number,
  }
}

export interface HeatingResponse {
  state: boolean,
  targetTemperature: number,
  actualTemperature: number,
}
