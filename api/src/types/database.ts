export enum SensorType {
    HEAT='HEAT',
    LIGHT='LIGHT',
    MOTION='MOTION'
}

export enum LightType {
    RGB='RGB',
    DIMABLE='DIMABLE',
    RGBDIMABLE='RGBDIMABLE',
    BASIC='BASIC',
}
