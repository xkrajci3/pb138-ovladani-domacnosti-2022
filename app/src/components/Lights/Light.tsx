import React, { useState } from "react";
import { HuePicker, AlphaPicker } from "react-color";

import "./light.css";

import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import LightbulbIcon from "@mui/icons-material/Lightbulb";
import Switch from "@mui/material/Switch";

import rgbHex from "rgb-hex";
import { IRgba, LightType } from "../../models/types";
import { useSWRConfig } from "swr";
import { updateLight } from "../../models/databaseRequests";

interface ILightProps {
  name: string;
  isOn: boolean;
  state: IRgba;
  type: LightType;
  roomName: string;
}

export const Light = ({ name, isOn, state, type, roomName }: ILightProps) => {
  const { mutate } = useSWRConfig();
  const [color, setColor] = useState(state);

  const handleChange = (colorObject: any) => {
    setColor(colorObject.rgb);
  };

  const handleChangeComplete = (colorObject: any) => {
    updateLight({ name, isOn, state: colorObject.rgb, roomName, mutate });
  };

  const handleSwitch = () => {
    updateLight({ name, isOn: !isOn, state, roomName, mutate });
  };

  return (
    <Box className="module" sx={{ padding: 1 }}>
      <Stack
        sx={{ height: "100%" }}
        direction="column"
        justifyContent="space-between"
      >
        <Stack direction="column">
          {type !== LightType.DIMABLE && (
            <>
              <Typography>Color</Typography>
              <Stack sx={{ pb: 1 }} direction="row">
                <HuePicker
                  color={color}
                  onChange={isOn ? handleChange : () => {}}
                  onChangeComplete={isOn ? handleChangeComplete : () => {}}
                  className="width"
                />
              </Stack>
            </>
          )}
          {type !== LightType.RGB && (
            <>
              <Typography>Intensity</Typography>
              <Stack direction="row">
                <AlphaPicker
                  color={color}
                  onChange={isOn ? handleChange : () => {}}
                  onChangeComplete={isOn ? handleChangeComplete : () => {}}
                  className="width"
                />
              </Stack>
            </>
          )}
        </Stack>
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Stack direction="row" alignItems="center">
            <LightbulbIcon
              sx={
                isOn
                  ? { color: `#${rgbHex(color.r, color.g, color.b, color.a)}` }
                  : {}
              }
            />
            <Typography>{name}</Typography>
          </Stack>
          <Stack direction="row" alignItems="center">
            <Typography>Off</Typography>
            <Switch
              checked={isOn}
              onChange={handleSwitch}
              name="switch"
              color="primary"
            />
            <Typography>On</Typography>
          </Stack>
        </Stack>
      </Stack>
    </Box>
  );
};
