import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Light } from "./Light";
import { LightType } from "../../models/types";

export default {
  title: "Light",
  component: Light,
} as ComponentMeta<typeof Light>;

const data = {
  state: { r: 241, g: 112, b: 19, a: 1 },
  name: "Bulb n.1",
  isOn: true,
  type: LightType.RGBDIMABLE,
};

const Template: ComponentStory<typeof Light> = () => (
  <Light
    name={""}
    isOn={false}
    state={{ r: 241, g: 112, b: 19, a: 1 }}
    type={LightType.RGB}
    roomName={""}
  />
);

export const LightDetail = Template.bind({});
