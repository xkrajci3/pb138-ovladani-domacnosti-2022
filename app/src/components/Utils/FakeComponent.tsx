import { Skeleton, Typography } from "@mui/material";

export const FakeComponent = () => {
  return (
    <div className="module">
      <Typography gutterBottom variant="body2">
        <Skeleton
          animation="wave"
          variant="rectangular"
          width={300}
          height={30}
        />
      </Typography>
      <Typography gutterBottom variant="body2">
        <Skeleton
          animation="wave"
          variant="rectangular"
          width={300}
          height={30}
        />
      </Typography>
      <Typography gutterBottom variant="body2">
        <Skeleton
          animation="wave"
          variant="rectangular"
          width={300}
          height={30}
        />
      </Typography>
    </div>
  );
};
