import React, { useState } from "react";
import Switch from "@mui/material/Switch";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import LightbulbIcon from "@mui/icons-material/Lightbulb";
import { DialogForm } from "../Dialog/DialogForm";
import { IRgba } from "../../models/types";
import { useSWRConfig } from "swr";
import { updateLight } from "../../models/databaseRequests";
import rgbHex from "rgb-hex";

interface IListItemLightProps {
  name: string;
  isOn: boolean;
  state: IRgba;
  roomName: string;
}

export const ListItemLight = ({
  name,
  isOn,
  state,
  roomName,
}: IListItemLightProps) => {
  const { mutate } = useSWRConfig();
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleSwitch = () => {
    updateLight({ name, isOn: !isOn, state, roomName, mutate });
  };

  return (
    <ListItem
      secondaryAction={
        <Switch
          checked={isOn}
          onChange={handleSwitch}
          name="switch"
          color="primary"
          aria-label="turn on/off the light"
        />
      }
    >
      <LightbulbIcon
        sx={
          isOn
            ? { color: `#${rgbHex(state.r, state.g, state.b, state.a)}` }
            : {}
        }
      />
      <ListItemButton onClick={handleClickOpen}>
        <ListItemText primary={name} />
      </ListItemButton>
      <DialogForm
        open={open}
        setOpen={setOpen}
        name={name}
        isOn={isOn}
        state={state}
        roomName={roomName}
      />
    </ListItem>
  );
};
