import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { LightList } from "./LightsList";

export default {
  title: "MainDashboardLights",
  component: LightList,
} as ComponentMeta<typeof LightList>;

const data = [
  {
    state: "ON",
    name: "Bulb n.1",
  },
  {
    state: "ON",
    name: "Bulb n.2",
  },
  {
    state: "OF",
    name: "Bulb n.3",
  },
  {
    state: "OF",
    name: "Bulb n.4",
  },
  {
    state: "ON",
    name: "Bulb n.5",
  },
];

const Template: ComponentStory<typeof LightList> = () => (
  <LightList header={""} lights={[]} roomName={""} />
);

export const Lights = Template.bind({});
