import React from "react";
import List from "@mui/material/List";
import ListSubheader from "@mui/material/ListSubheader";
import Skeleton from "@mui/material/Skeleton";
import { ILight } from "../../models/types";
import { ListItemLight } from "./ListItemLight";
import { v4 as uuidv4 } from "uuid";

interface IMainLightsProps {
  header: string;
  lights: ILight[];
  roomName: string;
}

export const LightList = ({ header, lights, roomName }: IMainLightsProps) => {
  if (!lights.length) return <></>;

  return (
    <div className="module">
      <List subheader={<ListSubheader>{header}</ListSubheader>}>
        {lights
          .sort((n1, n2) => n1.name.localeCompare(n2.name))
          .map((light) => {
            return (
              <ListItemLight {...light} roomName={roomName} key={uuidv4()} />
            );
          })}
      </List>
    </div>
  );
};

export default LightList;
