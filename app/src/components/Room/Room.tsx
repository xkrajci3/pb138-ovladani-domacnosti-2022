import { useParams } from "react-router-dom";
import useSWR from "swr";
import fetcher from "../../models/fetcher";
import { ILight, LightType } from "../../models/types";
import { Light } from "../Lights/Light";
import { v4 as uuidv4 } from "uuid";
import Heating from "../Heating/Heating";
import { Blinds } from "../Blinds/Blinds";
import LightList from "../LightsList/LightsList";
import { Skeleton, Typography } from "@mui/material";
import { FakeComponent } from "../Utils/FakeComponent";

export const Room = () => {
  const { name: roomName } = useParams();

  if (!roomName) return <div>Something went wrong</div>;

  const { data: dataLights, error: errorLights } = useSWR(
    `${import.meta.env.VITE_API}/api/room/lights?roomName=${roomName}`,
    fetcher
  );

  return (
    <>
      {!errorLights &&
        dataLights &&
        dataLights
          .sort((n1: ILight, n2: ILight) => n1.name.localeCompare(n2.name))
          .map((item: ILight) => {
            if (item.type !== LightType.BASIC)
              return (
                <div className="fill" key={uuidv4()}>
                  <Light {...item} roomName={roomName} key={uuidv4()} />
                </div>
              );
          })}
      {!errorLights && dataLights && (
        <div className="fill">
          <LightList
            header={"Standard lights"}
            lights={dataLights.filter((item: ILight) => {
              return item.type === LightType.BASIC;
            })}
            roomName={roomName}
          />
        </div>
      )}
      {errorLights && <div>failed to load lights</div>}
      {!dataLights && <FakeComponent />}
      <Heating roomName={roomName} />
      <Blinds roomName={roomName} />
    </>
  );
};
