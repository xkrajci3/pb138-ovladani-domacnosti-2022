import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";
import Chip from "@mui/material/Chip";
import Slider from "@mui/material/Slider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { SyntheticEvent, useState } from "react";
import useSWR, { useSWRConfig } from "swr";
import fetcher from "../../models/fetcher";
import { IBlinds } from "../../models/types";
import { v4 as uuidv4 } from "uuid";
import { updateBlinds } from "../../models/databaseRequests";
import Skeleton from "@mui/material/Skeleton";
import { FakeComponent } from "../Utils/FakeComponent";

const Step = 1;
const MinRoll = 0;
const MaxRoll = 10;

interface IBlindsProps {
  roomName: string;
}

export const Blinds = ({ roomName }: IBlindsProps) => {
  const { mutate } = useSWRConfig();
  const { data: dataBlinds, error: errorBlinds } = useSWR(
    `${import.meta.env.VITE_API}/api/room/blinds?roomName=${roomName}`,
    fetcher
  );

  if (errorBlinds) return <div>failed to load blinds</div>;
  if (!dataBlinds) return <FakeComponent />;

  const { state }: IBlinds = dataBlinds;

  function preventHorizontalKeyboardNavigation(event: React.KeyboardEvent) {
    if (event.key === "ArrowLeft" || event.key === "ArrowRight") {
      event.preventDefault();
    }
  }

  const handleChangeComplete = (
    event: Event | SyntheticEvent<Element, Event>,
    newValue: number | number[]
  ) => {
    updateBlinds({ state: newValue as number, roomName, mutate });
  };

  const handleFullRoll = (newValue: number) => {
    updateBlinds({ state: newValue, roomName, mutate });
  };

  return (
    <Box className="module" sx={{ padding: 1 }}>
      <Stack
        sx={{ height: "100%", padding: 2 }}
        direction="row"
        justifyContent="space-between"
      >
        <Slider
          sx={{
            '& input[type="range"]': {
              WebkitAppearance: "slider-vertical",
            },
          }}
          key={uuidv4()}
          orientation="vertical"
          aria-label="Blinds roll state"
          defaultValue={state}
          valueLabelDisplay="auto"
          step={Step}
          marks
          min={MinRoll}
          max={MaxRoll}
          onChangeCommitted={handleChangeComplete}
          onKeyDown={preventHorizontalKeyboardNavigation}
        />
        <Stack
          direction="column"
          justifyContent="space-between"
          alignItems="end"
        >
          <Stack direction="column" alignItems="end">
            <Typography>Roll state</Typography>
            <Chip label={`${state}`} />
          </Stack>

          <ButtonGroup
            orientation="vertical"
            aria-label="vertical contained button group"
            variant="contained"
          >
            <Button onClick={() => handleFullRoll(MaxRoll)}>Roll up</Button>
            <Button onClick={() => handleFullRoll(MinRoll)}>Roll down</Button>
          </ButtonGroup>
        </Stack>
      </Stack>
    </Box>
  );
};
