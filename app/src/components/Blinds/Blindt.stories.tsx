import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Blinds } from "./Blinds";

export default {
  title: "Blinds",
  component: Blinds,
} as ComponentMeta<typeof Blinds>;

const Template: ComponentStory<typeof Blinds> = () => <Blinds roomName={""} />;

export const NewBlinds = Template.bind({});
