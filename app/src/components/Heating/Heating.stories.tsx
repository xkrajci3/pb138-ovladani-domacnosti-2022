import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { Heating } from "./Heating";

export default {
  title: "Heating",
  component: Heating,
} as ComponentMeta<typeof Heating>;

const Template: ComponentStory<typeof Heating> = () => (
  <Heating roomName={""} />
);

export const Heat = Template.bind({});
