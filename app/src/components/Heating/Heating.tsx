import React, { SyntheticEvent, useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import Switch from "@mui/material/Switch";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Chip from "@mui/material/Chip";
import { IHeating } from "../../models/types";
import useSWR, { useSWRConfig } from "swr";
import fetcher from "../../models/fetcher";
import { v4 as uuidv4 } from "uuid";
import { updateHeating } from "../../models/databaseRequests";
import Skeleton from "@mui/material/Skeleton";
import { FakeComponent } from "../Utils/FakeComponent";

const MaxHeat = 30;
const MinHeat = 10;
const Step = 1;

interface IHeatingProps {
  roomName: string;
}

export const Heating = ({ roomName }: IHeatingProps) => {
  const { mutate } = useSWRConfig();
  const { data: dataHeating, error: errorHeating } = useSWR(
    `${import.meta.env.VITE_API}/api/room/heating?roomName=${roomName}`,
    fetcher
  );

  if (errorHeating) return <div>failed to load thermostat</div>;
  if (!dataHeating) return <FakeComponent />;

  useEffect(
    () => {
      const ws = new WebSocket(`${import.meta.env.VITE_WEBSOCKET}`);

      ws.onmessage = (webSocketMessage) => {
        mutate(`${import.meta.env.VITE_API}/api/room/heating?roomName=${roomName}`);
      };
      
      return () => {
        ws.close();
      }
    },
    []
  );
  

  const { actualTemperature, state, targetTemperature }: IHeating = dataHeating;
  const marks = [
    {
      value: MinHeat,
      label: `${MinHeat}°C`,
    },
    {
      value: targetTemperature,
      label: `${targetTemperature}°C`,
    },
    {
      value: MaxHeat,
      label: `${MaxHeat}°C`,
    },
  ];

  function preventHorizontalKeyboardNavigation(event: React.KeyboardEvent) {
    if (event.key === "ArrowLeft" || event.key === "ArrowRight") {
      event.preventDefault();
    }
  }

  const handleSwitch = () => {
    updateHeating({ state: !state, targetTemperature, roomName, mutate });
  };

  const handleChangeComplete = (
    event: Event | SyntheticEvent<Element, Event>,
    newTarget: number | number[]
  ) => {
    updateHeating({
      state,
      targetTemperature: newTarget as number,
      roomName,
      mutate,
    });
  };

  return (
    <Box className="module" sx={{ padding: 1 }}>
      <Stack
        sx={{ height: "100%", padding: 2 }}
        direction="row"
        justifyContent="space-between"
      >
        <Slider
          sx={{
            '& input[type="range"]': {
              WebkitAppearance: "slider-vertical",
            },
          }}
          key={uuidv4()}
          aria-label="Temperature"
          orientation="vertical"
          defaultValue={targetTemperature}
          step={Step}
          marks={marks}
          min={MinHeat}
          max={MaxHeat}
          valueLabelDisplay="auto"
          onKeyDown={preventHorizontalKeyboardNavigation}
          onChangeCommitted={handleChangeComplete}
          disabled={!state}
        />
        <Stack
          direction="column"
          justifyContent="space-between"
          alignItems="end"
        >
          <Stack direction="column" alignItems="end">
            <Typography>Room temperature</Typography>
            <Chip label={`${actualTemperature.toFixed(1)}°C`} />
          </Stack>
          <Stack direction="column" alignItems="end">
            <Typography>Thermostat</Typography>
            <Stack direction="row" alignItems="center">
              <Typography>Off</Typography>
              <Switch
                checked={state}
                onChange={handleSwitch}
                name="switch"
                color="primary"
              />
              <Typography>On</Typography>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    </Box>
  );
};

export default Heating;
