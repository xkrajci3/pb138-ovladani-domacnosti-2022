import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import useSWR, { mutate } from "swr";
import { capitalizeFirstLetter } from "../../models/capitalizeFirstLetter";
import fetcher from "../../models/fetcher";
import { v4 as uuidv4 } from "uuid";
import { Skeleton, CircularProgress } from "@mui/material";
import React, { useEffect } from "react";

interface ILandingTemperatureProps {
  roomsList: { name: string }[];
}

export const LandingTemperature = ({ roomsList }: ILandingTemperatureProps) => {
  const temperatureComponentList = roomsList.map((room) => {
    const { data: dataHeating, error: errorHeating } = useSWR(
      `${import.meta.env.VITE_API}/api/room/heating?roomName=${room.name}`,
      fetcher
    );

    useEffect(
      () => {
        const ws = new WebSocket(`${import.meta.env.VITE_WEBSOCKET}`);
  
        ws.onmessage = (webSocketMessage) => {
          mutate(`${import.meta.env.VITE_API}/api/room/heating?roomName=${room.name}`);
        };
        
        return () => {
          ws.close();
        }
      },
      []
    );

    if (errorHeating)
      return (
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          key={uuidv4()}
        >
          <Typography>{capitalizeFirstLetter(room.name)}</Typography>
		  <CircularProgress />
        </Stack>
      );
    if (!dataHeating)
      return (
        <div key={uuidv4()}>
          <Skeleton variant="rectangular" width={300} height={10} />
        </div>
      );

    return (
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        key={uuidv4()}
      >
        <Typography>{capitalizeFirstLetter(room.name)}</Typography>
        <Chip label={`${dataHeating.actualTemperature.toFixed(1)}°C`} />
      </Stack>
    );
  });

  return (
    <Box className="module" sx={{ padding: 1 }}>
      <Stack
        sx={{ height: "100%", width: "100%" }}
        direction="column"
        justifyContent="space-between"
      >
        {temperatureComponentList}
      </Stack>
    </Box>
  );
};
