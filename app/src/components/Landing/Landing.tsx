import useSWR from "swr";
import fetcher from "../../models/fetcher";
import { LandingControls } from "./LandingControls";
import { LandingLights } from "./LandingLights";
import { LandingTemperature } from "./LandingTemperature";

export const Landing = () => {
  const { data: dataRooms, error: errorRooms } = useSWR(
    `${import.meta.env.VITE_API}/api/rooms`,
    fetcher
  );

  if (errorRooms) return <div>failed to load room information</div>;
  if (!dataRooms) return <div>loading room information...</div>;

  return (
    <>
      <LandingLights roomsList={dataRooms} />
      <LandingTemperature roomsList={dataRooms} />
      <LandingControls roomsList={dataRooms} />
    </>
  );
};
