import useSWR from "swr";
import { capitalizeFirstLetter } from "../../models/capitalizeFirstLetter";
import fetcher from "../../models/fetcher";
import LightList from "../LightsList/LightsList";
import { v4 as uuidv4 } from "uuid";
import Skeleton from "@mui/material/Skeleton";
import Typography from "@mui/material/Typography";

interface ILandingLightsProps {
  roomsList: { name: string }[];
}

export const LandingLights = ({ roomsList }: ILandingLightsProps) => {
  const lightsComponentList = roomsList.map((room) => {
    const { data: dataLights, error: errorLights } = useSWR(
      `${import.meta.env.VITE_API}/api/room/lights?roomName=${room.name}`,
      fetcher
    );

    if (errorLights)
      return (
        <div key={uuidv4()}>
          failed to load information about {room.name} lights
        </div>
      );
    if (!dataLights)
      return (
        <div className="module" key={uuidv4()}>
          <Typography gutterBottom variant="body2">
            <Skeleton animation="wave" variant="rectangular" width={300} height={30} />
          </Typography>
          <Typography gutterBottom variant="body2">
            <Skeleton animation="wave" variant="rectangular" width={300} height={30} />
          </Typography>
          <Typography gutterBottom variant="body2">
            <Skeleton animation="wave" variant="rectangular" width={300} height={30} />
          </Typography>
        </div>
      );
    const header = capitalizeFirstLetter(room.name) + " lights";
    return (
      <LightList
        header={header}
        lights={dataLights}
        roomName={room.name}
        key={uuidv4()}
      />
    );
  });
  return <>{lightsComponentList}</>;
};
