import { Button, ButtonGroup, Typography } from "@mui/material";
import Stack from "@mui/material/Stack";
import { useSWRConfig } from "swr";
import { updateAllInGroup } from "../../models/databaseRequests";
import { ApiDeviceName } from "../../models/types";

interface ILandingControlProps {
  title: string;
  upper: string;
  lower: string;
  device: ApiDeviceName;
  roomsList?: { name: string }[];
}

export const LandingControl = ({
  title,
  upper,
  lower,
  device,
  roomsList,
}: ILandingControlProps) => {
  const { mutate } = useSWRConfig();

  const handleSwitch = (state: boolean, device: ApiDeviceName) => {
    updateAllInGroup({ state, device, roomsList, mutate });
  };

  return (
    <Stack
      sx={{ padding: 1 }}
      direction="row"
      justifyContent="space-between"
      alignItems="center"
    >
      <Typography>{title}</Typography>
      <ButtonGroup
        orientation="vertical"
        aria-label="vertical contained button group"
        variant="contained"
      >
        <Button onClick={() => handleSwitch(true, device)}>{upper}</Button>
        <Button onClick={() => handleSwitch(false, device)}>{lower}</Button>
      </ButtonGroup>
    </Stack>
  );
};
