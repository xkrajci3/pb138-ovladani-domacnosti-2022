import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import { ApiDeviceName } from "../../models/types";
import { LandingControl } from "./LandingControl";
import Skeleton from "@mui/material/Skeleton";
import { FakeComponent } from "../Utils/FakeComponent";

interface ILandingControls {
  roomsList: { name: string }[];
}

export const LandingControls = ({ roomsList }: ILandingControls) => {
  if (!roomsList) return <FakeComponent />;
  return (
    <Box className="module" sx={{ padding: 1 }}>
      <Stack
        sx={{ height: "100%", width: "100%" }}
        direction="column"
        justifyContent="space-between"
      >
        <LandingControl
          title={"Lights"}
          upper={"ON"}
          lower={"OFF"}
          device={ApiDeviceName.LIGTS}
          roomsList={roomsList}
        />
        <LandingControl
          title={"Central thermostat"}
          upper={"ON"}
          lower={"OFF"}
          device={ApiDeviceName.HEATING}
        />
        <LandingControl
          title={"Blinds"}
          upper={"UP"}
          lower={"DOWN"}
          device={ApiDeviceName.BLINDS}
        />
      </Stack>
    </Box>
  );
};
