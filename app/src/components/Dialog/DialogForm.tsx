import { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import { IRgba } from "../../models/types";
import { updateLightName } from "../../models/databaseRequests";
import { useSWRConfig } from "swr";

interface IDialogFormProps {
  open: boolean;
  setOpen: Function;
  name: string;
  isOn: boolean;
  state: IRgba;
  roomName: string;
}

export const DialogForm = ({
  open,
  setOpen,
  name,
  isOn,
  state,
  roomName,
}: IDialogFormProps) => {
  const { mutate } = useSWRConfig();
  const [newLightName, setNewName] = useState(name);

  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = () => {
    updateLightName({ currentLightName: name, newLightName, roomName, mutate });
    handleClose();
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewName(event.target.value);
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Submit</DialogTitle>
      <DialogContent>
        <DialogContentText>Enter new name for the light.</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Name"
          type="text"
          fullWidth
          variant="standard"
          value={newLightName}
          onChange={handleChange}
          inputProps={{ maxLength: 25 }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={onSubmit}>Submit</Button>
      </DialogActions>
    </Dialog>
  );
};
