import useSWR from "swr";
import fetcher from "./models/fetcher";
import { useState } from "react";
import "./App.css";
import {
  BrowserRouter,
  Route,
  Routes,
  NavLink,
  Navigate,
} from "react-router-dom";
import { Room } from "./components/Room/Room";
import { Landing } from "./components/Landing/Landing";
import { v4 as uuidv4 } from "uuid";
import { CircularProgress, LinearProgress } from "@mui/material";

function App() {
  const { data: dataRooms, error: errorRooms } = useSWR(
    `${import.meta.env.VITE_API}/api/rooms`,
    fetcher
  );

  if (errorRooms) return <div>failed to load room information</div>;
  if (!dataRooms) return (
    <>
      <LinearProgress />
      <div className="App">
        <nav className="menu">
          <div className="menu-item">
            <CircularProgress size={80} />
          </div>
        </nav>
        <div className="content">
          <div className="modules">
            <div className="module"></div>
          </div>
        </div>
      </div>
    </>
  );

  const roomMenuItems = dataRooms.map((room: { name: string }) => {
    const linkPath = "room/" + room.name;
    return (
      <NavLink className="menu-item" to={linkPath} key={uuidv4()}>
        {room.name}
      </NavLink>
    );
  });

  return (
    <div className="App">
      <BrowserRouter>
        <nav className="menu">
          <NavLink className="menu-item" to="main">
            Main
          </NavLink>
          {roomMenuItems}
        </nav>
        <div className="content">
          <div className="modules">
            <Routes>
              <Route path="/main" element={<Landing />} />
              <Route path="/room/:name" element={<Room />} />
              <Route path="*" element={<Navigate to="main" replace />} />
            </Routes>
          </div>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
