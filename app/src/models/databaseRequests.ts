import { ApiDeviceName, IRgba } from "./types";

interface IUpdateLight {
  name: string;
  isOn: boolean;
  state: IRgba;
  roomName: string;
  mutate: Function;
}

export const updateLight = async ({
  name,
  isOn,
  state,
  roomName,
  mutate,
}: IUpdateLight) => {
  const request = new Request(
    `${import.meta.env.VITE_API}/api/room/light?roomName=${roomName}`,
    {
      method: "PUT",
      body: JSON.stringify({ name, isOn, state }),
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  await fetch(request);
  mutate(`${import.meta.env.VITE_API}/api/room/lights?roomName=${roomName}`);
};

interface IUpdateLightName {
  currentLightName: string;
  newLightName: string;
  roomName: string;
  mutate: Function;
}

export const updateLightName = async ({
  currentLightName,
  newLightName,
  roomName,
  mutate,
}: IUpdateLightName) => {
  console.log("HERE");
  console.log(currentLightName);
  console.log(newLightName);
  const request = new Request(
    `${import.meta.env.VITE_API}/api/room/light/name?roomName=${roomName}`,
    {
      method: "PUT",
      body: JSON.stringify({ currentLightName, newLightName }),
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  await fetch(request);
  mutate(`${import.meta.env.VITE_API}/api/room/lights?roomName=${roomName}`);
};

interface IUpdateHeating {
  state: boolean;
  targetTemperature: number;
  roomName: string;
  mutate: Function;
}

export const updateHeating = async ({
  state,
  targetTemperature,
  roomName,
  mutate,
}: IUpdateHeating) => {
  const request = new Request(
    `${import.meta.env.VITE_API}/api/room/heating?roomName=${roomName}`,
    {
      method: "PUT",
      body: JSON.stringify({ state, targetTemperature }),
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  await fetch(request);
  mutate(`${import.meta.env.VITE_API}/api/room/heating?roomName=${roomName}`);
};

interface IUpdateBlinds {
  state: number;
  roomName: string;
  mutate: Function;
}

export const updateBlinds = async ({
  state,
  roomName,
  mutate,
}: IUpdateBlinds) => {
  const request = new Request(
    `${import.meta.env.VITE_API}/api/room/blinds?roomName=${roomName}`,
    {
      method: "PUT",
      body: JSON.stringify({ state }),
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  await fetch(request);
  mutate(`${import.meta.env.VITE_API}/api/room/blinds?roomName=${roomName}`);
};

interface ISwitchAllInGroup {
  device: ApiDeviceName;
  state: boolean;
  roomsList?: { name: string }[];
  mutate: Function;
}

export const updateAllInGroup = async ({
  device,
  state,
  roomsList,
  mutate,
}: ISwitchAllInGroup) => {
  const request = new Request(
    `${import.meta.env.VITE_API}/api/all/${device}?state=${state}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  await fetch(request);

  if (roomsList)
    roomsList.forEach((room) =>
      mutate(
        `${import.meta.env.VITE_API}/api/room/lights?roomName=${room.name}`
      )
    );
};
