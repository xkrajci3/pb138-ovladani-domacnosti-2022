export enum LightType {
  RGB = "RGB",
  DIMABLE = "DIMABLE",
  RGBDIMABLE = "RGBDIMABLE",
  BASIC = "BASIC",
}

export enum ApiDeviceName {
  LIGTS = "lights",
  HEATING = "heating",
  BLINDS = "blinds",
}

export interface ILightsData {
  state: string;
  name: string;
}

export interface IRgba {
  r: number;
  g: number;
  b: number;
  a: number;
}

export interface ILight {
  name: string;
  isOn: boolean;
  type: LightType;
  state: IRgba;
}

export interface IHeating {
  actualTemperature: number;
  state: boolean;
  targetTemperature: number;
}

export interface IBlinds {
  state: number;
}
