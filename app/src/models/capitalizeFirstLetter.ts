export const capitalizeFirstLetter = (item: string) => {
  if (item.length === 0) return item;
  return item.charAt(0).toUpperCase() + item.slice(1);
};
