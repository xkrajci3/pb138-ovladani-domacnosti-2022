import axios from 'axios';
//simple function to cause delay in specified ms
function delay(ms) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}


//updates sensors to different generated temperature every 10s
while (true) {
    await delay(10000);
    axios.get("http://localhost:5000/sensors/room/temperature").then(
        function(response) {
            console.log(response.data);
            response.data.forEach(async function(res) {
                const newTemperature = res.temperature + ((Math.floor(Math.random() * 201)) / 100) * (res.isOn ? +1 : -1);
                try {
                    await axios.put("http://localhost:5000/sensors/measurement", {value : newTemperature, roomId : res.roomId});
                    
                } catch {
                    console.log('Error updating sensor temperature');
                }
            });
        }
    )
    console.log("Updated temperature of sensors");
}


